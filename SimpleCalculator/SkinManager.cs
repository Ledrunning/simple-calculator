﻿using System.Windows;
using System.Windows.Controls;
using System.IO;
using System.Windows.Markup;
using System.Collections.Generic;


namespace SimpleCalculator
{
    public static class SkinManager
    {
        public static ResourceDictionary Current { get; private set; }
        static SkinManager()
        {
            Current = new ResourceDictionary();
        }
        public static ResourceDictionary GetSkin(string theme)
        {
            if (theme != null && File.Exists(theme))
                using (var fs = new FileStream(theme, FileMode.Open, FileAccess.Read, FileShare.Read))
                    return Current = (ResourceDictionary)XamlReader.Load(fs);
            return null;
        }
    }
}
            
